const form = document.getElementById('form');
const searchInput = document.getElementById('search');
const randomButton = document.getElementById('random-btn');
const searchButton = document.getElementById('search-btn');
const resultHeading = document.getElementById('result-heading');
const meals = document.getElementById('meals');
const singleMeal = document.getElementById('single-meal');


//Event handlers
function searchForMealByName(e){
    e.preventDefault();

    singleMeal.innerHTML = '';

    const searchQuery = searchInput.value;

    //check if search contains query
    if(searchQuery){

    //retrieve foods from api
        if(searchQuery.trim())
        {
            fetch(`https://www.themealdb.com/api/json/v1/1/search.php?s=${searchQuery}`)
            .then(result => result.json())
            .then(data => {
                console.log(data);

                //clear search input
                searchInput.value = '';

                //check if query has results
                if(data.meals != null)
                {
                    //clear heading
                    resultHeading.innerHTML = '';
                    
                    //render food name
                    resultHeading.innerHTML = `Search results for '${searchQuery}' :`;

                    //clear search results
                    meals.innerHTML = '';

                    //render result to dom
                    meals.innerHTML = data.meals.map(meal => 
                        `
                    <div class="meal">
                        <img src="${meal.strMealThumb}" />
                        <div class="meal-info" data-mealID="${meal.idMeal}">
                            <h3>${meal.strMeal}</h3>
                        </div>
                    </div>
                    `).join('');   
                }
               else
                {
                alert("No food found, please try again");
                }
            
            })            
        }
     
    }
    else{
        alert("Pleae enter a food");
    }  
}

function showRandomMeal(event){
    event.preventDefault();  
    
    meals.innerHTML = '';
    //retrieve random meal
    fetch(`https://www.themealdb.com/api/json/v1/1/random.php`)
    .then(response => response.json()
    .then(data => {
        const randomMeal = data.meals[0];
        addMealToDom(randomMeal);
        
    }))
}

function showMealInfo(event)
{
 
   // check if div clicked - has 'meal-info' - return div
   //event.path.find(childElement) - will return 'meal-info', 'meal', 'meals', 'container', 'body','html'
   const mealInfoDiv = event.path.find(childElement => {
            if(childElement.classList){ // if there are any elements with classes
                return childElement.classList.contains('meal-info'); //return class that contains 'meal-info'
            }else{
                return false;
            }
        }); 

        if(mealInfoDiv)
        {
            const mealID = mealInfoDiv.getAttribute('data-mealid');
            getMealById(mealID);
        }        
}

function getMealById(mealID){
    fetch(`https://www.themealdb.com/api/json/v1/1/lookup.php?i=${mealID}`)
        .then(response => response.json()
        .then(data => {
            const singleMeal = data.meals[0];
            if(singleMeal){
                addMealToDom(singleMeal);
            }
            
        } ))
}

function addMealToDom(meal){
    
    const ingredients = [];
    //parse meal ingredients to array
    for(let i=1; i <= 20; i++)
    {
        if(meal[`strIngredient${i}`]){ // if there is an ingredient
            
            ingredients.push(
                `${meal[`strMeasure${i}`]} - ${meal[`strIngredient${i}`]}`
                ); //ingredient unit, ingredient name
        }
        else{
            break; // end the loop
        }
    }

    
        singleMeal.innerHTML =
        `
            <div class="single-meal">

                <!-- Name and Image -->
                 <h1>${meal.strMeal}</h1>
                 <img src="${meal.strMealThumb}" />

                 <!-- Category and Type -->
                 <div class="single-meal-info">
                    ${meal.strCategory ? `<p>${meal.strCategory}</p>` : ''} <!-- Add category if exists -->
                    ${meal.strArea ? `<p>${meal.strArea}</p>` : ''} <!-- Add foot type if exists -->
                 </div>

                 <!-- Instructions -->
                 <div class="single-meal-instructions">
                    <h2>Instructions</h2>    
                   ${meal.strInstructions.split('.').map(element =>  
                       `<p>${element}</p>`
                   ).join('')}
                </div>

                <!-- Ingredients -->
                <div classs="single-meal-ingredients">
                    <h2>Ingredients</h2>
                    <ul>
                        ${ingredients.map(ingredient => `<li>${ingredient}</li>`).join('')}               
                    </ul>
                </div>
             </div>
        `


}

//Event listners
randomButton.addEventListener('click', showRandomMeal);
searchButton.addEventListener('click', searchForMealByName);
meals.addEventListener('click', showMealInfo);